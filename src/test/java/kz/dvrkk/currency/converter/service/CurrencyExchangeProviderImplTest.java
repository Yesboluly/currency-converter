package kz.dvrkk.currency.converter.service;

import kotlin.Pair;
import kz.dvrkk.currency.converter.model.ConversionRequest;
import kz.dvrkk.currency.converter.model.integration.CurrencyExchangeResponse;
import kz.dvrkk.currency.converter.service.impl.CurrencyExchangeProviderImpl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@ExtendWith(MockitoExtension.class)
class CurrencyExchangeProviderImplTest {

    private static MockWebServer mockWebServer;

    private static CurrencyExchangeProviderImpl underTest;

    @BeforeAll
    public static void setUp() {
        mockWebServer = new MockWebServer();
        WebClient mockedWebClient = WebClient.builder()
                .baseUrl(mockWebServer.url("/").toString())
                .build();
        underTest = new CurrencyExchangeProviderImpl(mockedWebClient);
    }

    @AfterAll
    public static void tearDown() throws IOException {
        mockWebServer.close();
    }

    @Test
    void exchange() {
        ConversionRequest request = new ConversionRequest();
        request.setFrom("EUR");
        request.setTo("USD");
        request.setAmount(BigDecimal.ONE);
        mockWebServer.enqueue(
                new MockResponse().setResponseCode(HttpStatus.OK.value())
                        .setHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .setBody(getBody())
        );

        CurrencyExchangeResponse currencyExchangeResponse = CurrencyExchangeResponse.builder()
                .baseCurrency("EUR")
                .date("2024-05-01")
                .rates(Map.of(
                        "AED", BigDecimal.valueOf(3.9d),
                        "EUR", BigDecimal.valueOf(1.0d),
                        "KZT", BigDecimal.valueOf(473.1d),
                        "USD", BigDecimal.valueOf(1.07d)
                ))
                .build();

        StepVerifier.create(underTest.exchange(request))
                .expectNext(new Pair<>(currencyExchangeResponse, "api.exchangerate-api.com"))
                .verifyComplete();
    }

    @Test
    void isAliveConnection() {
        mockWebServer.enqueue(
                new MockResponse().setResponseCode(HttpStatus.OK.value())
                        .setHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .setBody(getBody())
        );
        StepVerifier.create(underTest.isAliveConnection())
                .expectNext(true)
                .verifyComplete();
    }

    @NotNull
    private String getBody() {
        return """
                {
                    "base": "EUR",
                    "date": "2024-05-01",
                    "rates": {
                        "AED": 3.9,
                        "EUR": 1.0,
                        "KZT": 473.1,
                        "USD": 1.07
                    }
                }""";
    }
}
