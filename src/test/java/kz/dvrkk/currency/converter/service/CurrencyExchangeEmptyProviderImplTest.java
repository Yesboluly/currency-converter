package kz.dvrkk.currency.converter.service;

import kz.dvrkk.currency.converter.model.ConversionRequest;
import kz.dvrkk.currency.converter.service.impl.CurrencyExchangeEmptyProviderImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.test.StepVerifier;

import java.math.BigDecimal;

@ExtendWith(MockitoExtension.class)
class CurrencyExchangeEmptyProviderImplTest {

    private CurrencyExchangeEmptyProviderImpl underTest;

    @BeforeEach
    public void init() {
        underTest = new CurrencyExchangeEmptyProviderImpl();
    }

    @Test
    void exchange() {
        ConversionRequest request = new ConversionRequest();
        request.setFrom("EUR");
        request.setTo("USD");
        request.setAmount(BigDecimal.ONE);
        StepVerifier.create(underTest.exchange(request))
                .verifyError(IllegalStateException.class);
    }

    @Test
    void isAliveConnection() {
        StepVerifier.create(underTest.isAliveConnection())
                .expectNext(false)
                .verifyComplete();
    }
}
