package kz.dvrkk.currency.converter.service;

import kotlin.Pair;
import kz.dvrkk.currency.converter.model.ConversionRequest;
import kz.dvrkk.currency.converter.model.ConversionResponse;
import kz.dvrkk.currency.converter.model.integration.CurrencyExchangeResponse;
import kz.dvrkk.currency.converter.service.impl.CurrencyConverterImpl;
import kz.dvrkk.currency.converter.service.impl.CurrencyExchangeProviderImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.util.Map;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CurrencyConverterImplTest {

    @Mock
    private CurrencyExchangeProviderFactory factory;

    @Mock
    private CurrencyExchangeProviderImpl mainProvider;

    private CurrencyConverterImpl underTest;

    @BeforeEach
    public void init() {
        underTest = new CurrencyConverterImpl(factory);
    }

    @Test
    void convert() {
        ConversionRequest request = new ConversionRequest();
        request.setFrom("EUR");
        request.setTo("USD");
        request.setAmount(BigDecimal.ONE);
        CurrencyExchangeResponse currencyExchangeResponse =
                CurrencyExchangeResponse.builder()
                        .baseCurrency("EUR")
                        .date("2024-05-02")
                        .rates(
                                Map.of(
                                        "AED", BigDecimal.valueOf(3.9d),
                                        "EUR", BigDecimal.valueOf(1.0d),
                                        "KZT", BigDecimal.valueOf(473.1d),
                                        "USD", BigDecimal.valueOf(1.07d)
                                )
                        )
                        .build();
        ConversionResponse conversionResponse = new ConversionResponse();
        conversionResponse.setFrom("EUR");
        conversionResponse.setTo("USD");
        conversionResponse.setAmount(BigDecimal.ONE);
        conversionResponse.setConverted(BigDecimal.valueOf(1.07d));

        when(factory.getImplementation()).thenReturn(Mono.just(mainProvider));
        when(mainProvider.exchange(request)).thenReturn(Mono.just(new Pair<>(currencyExchangeResponse, "test-api.com")));

        StepVerifier.create(underTest.convert(request))
                .expectNext(conversionResponse)
                .verifyComplete();
    }
}
