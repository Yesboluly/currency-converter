package kz.dvrkk.currency.converter.service;

import kz.dvrkk.currency.converter.service.impl.CurrencyExchangeEmptyProviderImpl;
import kz.dvrkk.currency.converter.service.impl.CurrencyExchangeProviderFactoryImpl;
import kz.dvrkk.currency.converter.service.impl.CurrencyExchangeProviderImpl;
import kz.dvrkk.currency.converter.service.impl.CurrencyExchangeSecondaryProviderImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Objects;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CurrencyExchangeProviderFactoryImplTest {

    @Mock
    private CurrencyExchangeProviderImpl mainProvider;

    @Mock
    private CurrencyExchangeSecondaryProviderImpl secondaryProvider;

    @Mock
    private CurrencyExchangeEmptyProviderImpl emptyProvider;

    private CurrencyExchangeProviderFactoryImpl underTest;

    @BeforeEach
    public void init() {
        underTest = new CurrencyExchangeProviderFactoryImpl(mainProvider, secondaryProvider, emptyProvider);
    }

    @Test
    void exchangeWhenRandomProvider() {
        when(mainProvider.isAliveConnection()).thenReturn(Mono.just(true));
        when(secondaryProvider.isAliveConnection()).thenReturn(Mono.just(true));

        StepVerifier.create(underTest.getImplementation())
                .expectNextMatches(Objects::nonNull)
                .verifyComplete();
    }

    @Test
    void exchangeWhenMainProviderIsOffline() {
        when(mainProvider.isAliveConnection()).thenReturn(Mono.just(false));
        when(secondaryProvider.isAliveConnection()).thenReturn(Mono.just(true));

        StepVerifier.create(underTest.getImplementation())
                .expectNext(secondaryProvider)
                .verifyComplete();
    }

    @Test
    void exchangeWhenSecondaryProviderIsOffline() {
        when(mainProvider.isAliveConnection()).thenReturn(Mono.just(true));
        when(secondaryProvider.isAliveConnection()).thenReturn(Mono.just(false));

        StepVerifier.create(underTest.getImplementation())
                .expectNext(mainProvider)
                .verifyComplete();
    }

    @Test
    void exchangeWhenBothProvidersOffline() {
        when(mainProvider.isAliveConnection()).thenReturn(Mono.just(false));
        when(secondaryProvider.isAliveConnection()).thenReturn(Mono.just(false));

        StepVerifier.create(underTest.getImplementation())
                .expectNext(emptyProvider)
                .verifyComplete();
    }
}
