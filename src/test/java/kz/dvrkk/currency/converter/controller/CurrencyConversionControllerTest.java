package kz.dvrkk.currency.converter.controller;

import kz.dvrkk.currency.converter.model.ConversionRequest;
import kz.dvrkk.currency.converter.service.CurrencyConverter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import java.math.BigDecimal;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = CurrencyConversionController.class)
class CurrencyConversionControllerTest {

    @MockBean
    CurrencyConverter currencyConverter;

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void convert() {
        ConversionRequest request = new ConversionRequest();
        request.setFrom("EUR");
        request.setTo("USD");
        request.setAmount(BigDecimal.ONE);
        webTestClient.post()
                .uri("/currency/convert")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(request))
                .exchange()
                .expectStatus().isOk();

        verify(currencyConverter, times(1)).convert(request);
    }

}
