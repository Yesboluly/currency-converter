package kz.dvrkk.currency.converter.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfig {

    @Value("${app.integration.exchange-rate.url}")
    private String exchangeRateUrl;

    @Value("${app.integration.exchange-rate-secondary.url}")
    private String exchangeRateSecondaryUrl;

    @Bean
    public WebClient exchangeRateWebClient() {
        return WebClient.builder()
                .baseUrl(exchangeRateUrl)
                .build();
    }

    @Bean
    public WebClient exchangeRateSecondaryWebClient() {
        return WebClient.builder()
                .baseUrl(exchangeRateSecondaryUrl)
                .build();
    }
}
