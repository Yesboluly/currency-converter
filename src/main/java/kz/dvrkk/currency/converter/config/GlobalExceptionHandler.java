package kz.dvrkk.currency.converter.config;

import kz.dvrkk.currency.converter.exception.CurrencyNotProvidedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({CurrencyNotProvidedException.class})
    public ResponseEntity<Object> handle(CurrencyNotProvidedException exception) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(exception.getMessage());
    }
}
