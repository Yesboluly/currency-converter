package kz.dvrkk.currency.converter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ConversionRequest extends BaseConversionDto {

}