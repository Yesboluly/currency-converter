package kz.dvrkk.currency.converter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public class ConversionResponse extends BaseConversionDto {
    BigDecimal converted;
}
