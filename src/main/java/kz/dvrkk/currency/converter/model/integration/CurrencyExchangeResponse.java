package kz.dvrkk.currency.converter.model.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.util.Map;

@Value
@Builder
public class CurrencyExchangeResponse {
    @JsonProperty("base")
    String baseCurrency;
    String date;
    Map<String, BigDecimal> rates;
}
