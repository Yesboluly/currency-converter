package kz.dvrkk.currency.converter.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BaseConversionDto {
    protected String from;
    protected String to;
    protected BigDecimal amount;
}
