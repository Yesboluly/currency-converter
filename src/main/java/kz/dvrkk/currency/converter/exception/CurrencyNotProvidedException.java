package kz.dvrkk.currency.converter.exception;

public class CurrencyNotProvidedException extends RuntimeException {

    public CurrencyNotProvidedException(String currency, String apiHost) {
        super("Currency you requested to convert - " +
                currency + " is not provided by " + apiHost + " API");
    }
}
