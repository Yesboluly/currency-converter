package kz.dvrkk.currency.converter.controller;

import kz.dvrkk.currency.converter.model.ConversionRequest;
import kz.dvrkk.currency.converter.model.ConversionResponse;
import kz.dvrkk.currency.converter.service.CurrencyConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/currency/convert")
@RequiredArgsConstructor
public class CurrencyConversionController {

    private final CurrencyConverter currencyConverter;

    @PostMapping
    public Mono<ConversionResponse> convert(@RequestBody ConversionRequest request) {
        return currencyConverter.convert(request);
    }
}
