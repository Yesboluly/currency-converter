package kz.dvrkk.currency.converter.service;

import kz.dvrkk.currency.converter.model.ConversionRequest;
import kz.dvrkk.currency.converter.model.ConversionResponse;
import reactor.core.publisher.Mono;

public interface CurrencyConverter {
    Mono<ConversionResponse> convert(ConversionRequest request);
}
