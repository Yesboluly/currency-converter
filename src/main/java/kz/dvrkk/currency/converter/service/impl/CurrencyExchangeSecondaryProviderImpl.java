package kz.dvrkk.currency.converter.service.impl;

import kotlin.Pair;
import kz.dvrkk.currency.converter.model.ConversionRequest;
import kz.dvrkk.currency.converter.model.integration.CurrencyExchangeResponse;
import kz.dvrkk.currency.converter.service.CurrencyExchangeProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Currency;

@Service
@RequiredArgsConstructor
public class CurrencyExchangeSecondaryProviderImpl implements CurrencyExchangeProvider {

    @Qualifier("exchangeRateSecondaryWebClient")
    private final WebClient webClient;

    @Value("${app.integration.exchange-rate-secondary.access-key}")
    private String accessKey;

    @Override
    @Cacheable(value = "secondaryCurrencyExchangeResponse", key = "#request.from")
    public Mono<Pair<CurrencyExchangeResponse, String>> exchange(ConversionRequest request) {
        Currency currencyFrom = Currency.getInstance(request.getFrom());
        String currencyFromCode = currencyFrom.getCurrencyCode();
        return webClient.get()
                .uri("/latest?base={base}&access_key={access_key}", currencyFromCode, accessKey)
                .retrieve()
                .bodyToMono(CurrencyExchangeResponse.class)
                .map(response -> new Pair<>(response, "api.exchangeratesapi.io"));
    }

    @Override
    public Mono<Boolean> isAliveConnection() {
        return webClient.get()
                .uri("/latest?base=EUR&access_key={access_key}", accessKey)
                .retrieve()
                .toEntity(CurrencyExchangeResponse.class)
                .map(e -> !e.getStatusCode().isError());
    }
}
