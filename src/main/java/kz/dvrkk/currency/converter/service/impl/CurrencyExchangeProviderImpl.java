package kz.dvrkk.currency.converter.service.impl;

import kotlin.Pair;
import kz.dvrkk.currency.converter.model.ConversionRequest;
import kz.dvrkk.currency.converter.model.integration.CurrencyExchangeResponse;
import kz.dvrkk.currency.converter.service.CurrencyExchangeProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Currency;

@Service
@RequiredArgsConstructor
public class CurrencyExchangeProviderImpl implements CurrencyExchangeProvider {

    @Qualifier("exchangeRateWebClient")
    private final WebClient webClient;

    @Override
    @Cacheable(value = "mainCurrencyExchangeResponse", key = "#request.from")
    public Mono<Pair<CurrencyExchangeResponse, String>> exchange(ConversionRequest request) {
        Currency currencyFrom = Currency.getInstance(request.getFrom());
        String currencyFromCode = currencyFrom.getCurrencyCode();
        return webClient.get()
                .uri("/v4/latest/" + currencyFromCode)
                .retrieve()
                .bodyToMono(CurrencyExchangeResponse.class)
                .map(response -> new Pair<>(response, "api.exchangerate-api.com"));
    }

    @Override
    public Mono<Boolean> isAliveConnection() {
        return webClient.get()
                .uri("/v4/latest/EUR")
                .retrieve()
                .toEntity(CurrencyExchangeResponse.class)
                .map(e -> !e.getStatusCode().isError());
    }
}
