package kz.dvrkk.currency.converter.service.impl;

import kz.dvrkk.currency.converter.service.CurrencyExchangeProvider;
import kz.dvrkk.currency.converter.service.CurrencyExchangeProviderFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Random;

@Component
@RequiredArgsConstructor
public class CurrencyExchangeProviderFactoryImpl implements CurrencyExchangeProviderFactory {

    private final CurrencyExchangeProviderImpl mainProvider;

    private final CurrencyExchangeSecondaryProviderImpl secondaryProvider;

    private final CurrencyExchangeEmptyProviderImpl emptyProvider;

    @Override
    public Mono<CurrencyExchangeProvider> getImplementation() {
        return mainProvider.isAliveConnection()
                .zipWith(secondaryProvider.isAliveConnection())
                .map(aliveConnections -> {
                            boolean mainProviderIsAlive = aliveConnections.getT1();
                            boolean secondaryProviderIsAlive = aliveConnections.getT2();
                            if (!mainProviderIsAlive && !secondaryProviderIsAlive) {
                                return emptyProvider;
                            }
                            if (!mainProviderIsAlive) {
                                return secondaryProvider;
                            }
                            if (!secondaryProviderIsAlive) {
                                return mainProvider;
                            }
                            return getRandomProvider();
                        }
                );
    }

    private CurrencyExchangeProvider getRandomProvider() {
        return new Random().nextBoolean() ? mainProvider : secondaryProvider;
    }
}
