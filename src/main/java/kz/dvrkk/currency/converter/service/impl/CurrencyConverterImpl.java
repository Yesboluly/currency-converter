package kz.dvrkk.currency.converter.service.impl;

import kz.dvrkk.currency.converter.exception.CurrencyNotProvidedException;
import kz.dvrkk.currency.converter.model.ConversionRequest;
import kz.dvrkk.currency.converter.model.ConversionResponse;
import kz.dvrkk.currency.converter.service.CurrencyConverter;
import kz.dvrkk.currency.converter.service.CurrencyExchangeProviderFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class CurrencyConverterImpl implements CurrencyConverter {

    private final CurrencyExchangeProviderFactory factory;

    @Override
    public Mono<ConversionResponse> convert(ConversionRequest request) {
        return factory.getImplementation()
                .flatMap(providerImpl -> providerImpl.exchange(request))
                .map(pair -> {
                            Currency currencyFrom = Currency.getInstance(request.getFrom());
                            Currency currencyTo = Currency.getInstance(request.getTo());
                            String currencyFromCode = currencyFrom.getCurrencyCode();
                            String currencyToCode = currencyTo.getCurrencyCode();
                            BigDecimal amount = request.getAmount();
                            Map<String, BigDecimal> ratesMap = pair.getFirst().getRates();

                            if (!ratesMap.containsKey(currencyToCode)) {
                                throw new CurrencyNotProvidedException(currencyToCode, pair.getSecond());
                            }

                            BigDecimal value = ratesMap.get(currencyToCode);
                            BigDecimal converted = amount.multiply(value);
                            ConversionResponse response = new ConversionResponse();
                            response.setFrom(currencyFromCode);
                            response.setTo(currencyToCode);
                            response.setAmount(amount);
                            response.setConverted(converted);
                            return response;
                        }
                );
    }
}
