package kz.dvrkk.currency.converter.service;

import reactor.core.publisher.Mono;

public interface CurrencyExchangeProviderFactory {
    Mono<CurrencyExchangeProvider> getImplementation();
}
