package kz.dvrkk.currency.converter.service.impl;

import kotlin.Pair;
import kz.dvrkk.currency.converter.model.ConversionRequest;
import kz.dvrkk.currency.converter.model.integration.CurrencyExchangeResponse;
import kz.dvrkk.currency.converter.service.CurrencyExchangeProvider;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class CurrencyExchangeEmptyProviderImpl implements CurrencyExchangeProvider {

    @Override
    public Mono<Pair<CurrencyExchangeResponse, String>> exchange(ConversionRequest request) {
        return Mono.error(new IllegalStateException("There are no providers available for exchange rate, please try later"));
    }

    @Override
    public Mono<Boolean> isAliveConnection() {
        return Mono.just(false);
    }
}
