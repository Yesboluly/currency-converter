package kz.dvrkk.currency.converter.service;

import kotlin.Pair;
import kz.dvrkk.currency.converter.model.ConversionRequest;
import kz.dvrkk.currency.converter.model.integration.CurrencyExchangeResponse;
import reactor.core.publisher.Mono;

public interface CurrencyExchangeProvider {
    Mono<Pair<CurrencyExchangeResponse, String>> exchange(ConversionRequest request);

    Mono<Boolean> isAliveConnection();
}
