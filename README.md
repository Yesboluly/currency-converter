# Currency Converter App

### Application
This App has only one Reactive REST-Endpoint to convert the currencies, here is the curl for it: 

```shell
curl --location 'http://localhost:8080/currency/convert' \
--header 'Content-Type: application/json' \
--data '{
"from": "EUR",
"to": "USD",
"amount": 1
}'
```

### How to run
Run the following command in your terminal:

```shell
mvn spring-boot:run
```
